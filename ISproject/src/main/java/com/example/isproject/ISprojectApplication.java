package com.example.isproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ISprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ISprojectApplication.class, args);
    }

}
