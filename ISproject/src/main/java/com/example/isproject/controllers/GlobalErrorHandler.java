package com.example.isproject.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.server.ResponseStatusException;

import com.example.isproject.api.entities.ErrorInfo;
import com.example.isproject.internals.MyRestException;


@Controller
@ControllerAdvice
public class GlobalErrorHandler implements ErrorController {
    private final ErrorAttributes errorAttributes;

    public GlobalErrorHandler(ErrorAttributes errorAttrs) {
        errorAttributes = errorAttrs;
    }

    @RequestMapping("/error")
    public ResponseEntity<ErrorInfo> handleError(final HttpServletRequest request, final HttpServletResponse response) {
        ServletWebRequest webRequest = new ServletWebRequest(request);
        Throwable cause = errorAttributes.getError(webRequest);

        if (cause != null) {
            int statusCode;
            if (cause instanceof MyRestException) {
                statusCode = ((MyRestException) cause).getStatus().value();
            } else if (cause instanceof ResponseStatusException) {
                statusCode = ((ResponseStatusException) cause).getRawStatusCode();
            } else {
                statusCode = response.getStatus();
                if (statusCode < 400) {
                    statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
                }
            }
            return ResponseEntity.status(statusCode).body(new ErrorInfo(cause));
        } else {
            return null;
        }
    }
}










