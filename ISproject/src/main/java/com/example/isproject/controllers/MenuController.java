package com.example.isproject.controllers;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import com.example.isproject.db.entities.Dish;
import com.example.isproject.internals.DishesService;
import com.example.isproject.internals.ImageData;
import com.example.isproject.internals.MyRestException;


@Controller
@RequestMapping("/dishes")
public class MenuController {


    private final DishesService dishesService;


    public MenuController(DishesService dishesSvc) {
        dishesService = dishesSvc;
    }

    //
    // READ
    //

    @GetMapping("")
    @ResponseBody
    public List<Dish> getAllDishes() {
        return dishesService.findAll();
    }

    @GetMapping(path = "", params = {"view=html"})
    @ResponseBody
    public ModelAndView getAllDishesHtmlView() {
        List<Dish> dishes = dishesService.findAll();
        return new ModelAndView("menu", "dishes", dishes);
    }


    @GetMapping("/{id}/image")
    public ResponseEntity<byte[]> findDishImageById(@PathVariable long id) {
        try {
            return dishesService.findImageById(id)
                .map(e -> ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.parseMediaType(e.contentType))
                    .header("Content-Disposition", "inline; filename=" + e.name).body(e.content)
                ).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (MyRestException ex) {
            throw ex.wrapWithResponseStatusException("Fail to get dish image");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Dish> findDishById(@PathVariable long id) {
        return dishesService.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    // CREATE

    @PostMapping(path = "", params = {"action=create"}, consumes = "multipart/form-data")
    public ResponseEntity<Dish> createNewDish(@RequestParam String name, @RequestParam String description, @RequestParam Double kcal,
                                              @RequestParam Double price, @RequestParam MultipartFile imageFile) {
        try {
            Dish newDish = dishesService.create(name, description, kcal, price, makeImageData(imageFile));
            return ResponseEntity.created(URI.create("/dishes/" + newDish.getId())).body(newDish);
        } catch (MyRestException ex) {
            throw ex.wrapWithResponseStatusException("Fail to create a new dish");
        }
    }

    private ImageData makeImageData(MultipartFile imageFile) throws MyRestException {
        try {
            if (imageFile != null && imageFile.getSize() > 0) {
                String name = imageFile.getOriginalFilename();
                byte[] data = imageFile.getBytes();
                String contentType = imageFile.getContentType();
                return new ImageData(name, data, contentType);
            } else {
                return null;
            }
        } catch (IOException ex) {
            throw new MyRestException(HttpStatus.INTERNAL_SERVER_ERROR, "Request image cannot be retrieved due to the storage access error", ex);
        }
    }

    @GetMapping(path = "", params = {"action=create", "mode=htmlForm"})
    @ResponseBody
    public ModelAndView getCreateNewDishHtmlForm() {
        return new ModelAndView("dishinfo", "dish", new Dish(null, null, null, null, null, null, null));
    }

    //
    // UPDATE
    //

    @PostMapping(value = "/{id}", params = {"action=update"}, consumes = "multipart/form-data")
    public ResponseEntity<Dish> updateDish(@PathVariable long id, @RequestParam String name, @RequestParam String description,
                                           @RequestParam Double kcal, @RequestParam Double price, @RequestParam MultipartFile imageFile) {
        try {
            Optional<Dish> updatedDish = dishesService.updateIfExists(id, name, description, kcal, price, makeImageData(imageFile));
            return updatedDish.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (MyRestException e) {
            throw e.wrapWithResponseStatusException("Failed to update dish #" + id);
        }
    }

    @GetMapping(path = "/{id}", params = {"action=edit", "mode=htmlForm"})
    @ResponseBody
    public ModelAndView getEditDishHtmlForm(@PathVariable long id) {
        Optional<Dish> dish = dishesService.findById(id);
        if (dish.isPresent()) {
            return new ModelAndView("dishinfo", "dish", dish.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such dish with id" + id);
        }

    }

    //
    // DELETE
    //

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteDish(@PathVariable long id) {
        return dishesService.deleteIfExists(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @PostMapping(value = "/{id}", params = {"action=delete"})
    public ResponseEntity<Object> deleteDishWithPost(@PathVariable long id) {
        return this.deleteDish(id);
    }

    @GetMapping(path = "/{id}", params = {"action=delete", "mode=htmlForm"})
    @ResponseBody
    public ModelAndView getDeleteDishHtmlConfirmationForm(@PathVariable long id) {
        Optional<Dish> dish = dishesService.findById(id);
        if (dish.isPresent()) {
            return new ModelAndView("dishDeleteConfirmation", "dish", dish.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such dish with id" + id);
        }
    }

}
