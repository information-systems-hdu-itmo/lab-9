package com.example.isproject.internals;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.isproject.db.entities.Dish;
import com.example.isproject.db.entities.Image;
import com.example.isproject.db.repositories.DishesRepository;

@Service
public class DishesService {

    private final DishesRepository dishesRepository;
    private final ImagesService imagesService;

    public DishesService(DishesRepository dishesDb, ImagesService imageService) {
        dishesRepository = dishesDb;
        imagesService = imageService;
    }

    public List<Dish> findAll() {
        return dishesRepository.findAll();
    }

    public Dish create(String name, String description, Double kcal, Double price, ImageData imageData) throws MyRestException {
        Image image = imageData == null ? null : imagesService.createImage(imageData);
        Dish newDish = new Dish(null, name, description, kcal, price, image == null ? null : image.getId(), null);
        return dishesRepository.save(newDish);
    }

    public Optional<Dish> findById(long id) {
        return dishesRepository.findById(id);
    }

    public Optional<ImageData> findImageById(long id) throws MyRestException {
        Optional<Long> imageId = dishesRepository.findById(id).flatMap(d -> Optional.ofNullable(d.getImageId()));
        if (imageId.isPresent()) {
            return imagesService.findById(imageId.get());
        } else {
            return Optional.empty();
        }
    }

    public Optional<Dish> updateIfExists(long id, String name, String description, Double kcal, Double price, ImageData imageData) throws MyRestException {
        Optional<Dish> oldDish = dishesRepository.findById(id);
        if (oldDish.isPresent()) {
            Dish dish = oldDish.get();
            if (name != null) {
                dish.setName(name);
            }
            if (description != null) {
                dish.setDescription(description);
            }
            if (kcal != null)
                dish.setKcal(kcal);
            if (price != null) {
                dish.setPrice(price);
            }
            if (imageData != null) {
                Image img = dish.getImageId() == null ? imagesService.createImage(imageData) : imagesService.update(dish.getId(), imageData);
                dish.setImageId(img.getId());
            }
            return Optional.of(dishesRepository.save(dish));
        } else {
            return Optional.empty();
        }
    }

    public boolean deleteIfExists(long id) {
        Optional<Dish> dish = dishesRepository.findById(id);
        if (dish.isPresent()) {
            dishesRepository.delete(dish.get());
            return true;
        } else {
            return false;
        }

    }

}
