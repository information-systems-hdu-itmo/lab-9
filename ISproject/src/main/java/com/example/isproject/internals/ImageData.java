package com.example.isproject.internals;

public class ImageData {
    public final String name;
    public final byte[] content;
    public final String contentType;

    public ImageData(String name, byte[] content, String contentType) {
        this.name = name;
        this.content = content;
        this.contentType = contentType;
    }
}
