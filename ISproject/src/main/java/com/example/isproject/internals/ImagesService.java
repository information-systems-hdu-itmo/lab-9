package com.example.isproject.internals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.isproject.db.entities.Image;
import com.example.isproject.db.repositories.ImagesRepository;

@Service
public class ImagesService {

    private final ImagesRepository imagesRepository;
    private final Path uploadRoot;

    public ImagesService(ImagesRepository imagesDb) {
        imagesRepository = imagesDb;
        uploadRoot = this.prepareUploadRootPath(System.getenv("UPLOAD_LOCATION"));
    }

    public Image createImage(ImageData imageData) throws MyRestException {
        Image image = imagesRepository.save(new Image(null, imageData.name, null, imageData.contentType));
        try {
            return this.storeImageData(image, imageData);
        } catch (IOException ex) {
            imagesRepository.delete(image);
            throw new MyRestException(HttpStatus.SERVICE_UNAVAILABLE, "Fail to preserve uploaded image data", ex);
        }
    }

    private Image storeImageData(Image image, ImageData imageData) throws IOException, MyRestException {
        String localFileName = image.getId() + extractFileNameExtension(imageData.name);
        Path localFilePath = uploadRoot.resolve(localFileName);

        String localTempFile = localFileName + ".tmp";

        Path localTempFilePath = uploadRoot.resolve(localTempFile);

        Files.deleteIfExists(localTempFilePath);
        Files.write(localTempFilePath, imageData.content, StandardOpenOption.CREATE_NEW);

        if (image.getStorageFileName() != null) {
            String oldLocalFileName = image.getStorageFileName();
            Path oldLocalFile = uploadRoot.resolve(oldLocalFileName);
            try {
                Files.deleteIfExists(oldLocalFile);
            } catch (IOException ex) {
                Files.deleteIfExists(localTempFilePath);
                throw new MyRestException(HttpStatus.SERVICE_UNAVAILABLE, "Fail to delete old image data", ex);
            }
            image.setStorageFileName(null);
            image = imagesRepository.save(image);
        }
        Files.move(localTempFilePath, localFilePath);
        image.setOriginalFileName(imageData.name);
        image.setStorageFileName(localFileName);
        image.setMimeType(Files.probeContentType(localFilePath));
        return imagesRepository.save(image);
    }

    public Optional<ImageData> findById(long id) throws MyRestException {
        Optional<Image> info = imagesRepository.findById(id);
        if (info.isPresent()) {
            Image image = info.get();
            if (image.getStorageFileName() != null) {
                Path localFile = uploadRoot.resolve(image.getStorageFileName());
                try {
                    byte[] rawData = Files.readAllBytes(localFile);
                    return Optional.of(new ImageData(image.getOriginalFileName(), rawData, image.getMimeType()));
                } catch (IOException ex) {
                    throw new MyRestException(HttpStatus.SERVICE_UNAVAILABLE, "Fail to read image data", ex);
                }
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    public Image update(long imageId, ImageData imageData) throws MyRestException {
        Optional<Image> image = imagesRepository.findById(imageId);
        if (image.isPresent()) {
            try {
                return this.storeImageData(image.get(), imageData);
            } catch (IOException ex) {
                throw new MyRestException(HttpStatus.SERVICE_UNAVAILABLE, "Fail to preserve upload image data", ex);
            }
        } else {
            throw new MyRestException(HttpStatus.NOT_FOUND, "No such image exists");
        }
    }

    private Path prepareUploadRootPath(String pathString) {
        if (pathString != null) {
            Path path = Path.of(pathString);
            if (Files.isDirectory(path)) {
                return path;
            }
        }
        throw new RuntimeException("Images upload location is not specified! Check the UPLOAD_LOCATION environment variable.");
    }

    private String extractFileNameExtension(String name) {
        String ext = ".";
        if (name != null) {
            int n = name.lastIndexOf('.');
            if (n != -1) {
                ext = name.substring(n).replaceAll("[^a-zA-Z0-9-_\\.]", "_");
            }
        }
        return ext;
    }

}
