package com.example.isproject.internals;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class MyRestException extends Exception {

    private final HttpStatus status;

    public MyRestException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public MyRestException(HttpStatus status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ResponseStatusException wrapWithResponseStatusException(String message) {
        return new ResponseStatusException(this.getStatus(), message, this);
    }
}
